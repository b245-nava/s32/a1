const http = require('http');

let port = 4000;

http.createServer((req, res) =>{

	// Welcome
	if(req.url === "/" && req.method === "GET"){
		res.writeHead(200, {'Content-Type': 'text/plain'});
		res.write("Welcome to the Booking System!");
		res.end();
	}
	// Profile Page + Get
	else if(req.url === "/profile" && req.method === "GET"){
		res.writeHead(200, {'Content-Type': 'text/plain'});
		res.write("Welcome to your profile!");
		res.end();
	}
	// Courses Page + Get
	else if(req.url === "/courses" && req.method === "GET"){
		res.writeHead(200, {'Content-Type': 'text/plain'});
		res.write("Here are our available courses.");
		res.end();
	}
	// addCourse + POST
	else if(req.url === "/addCourse" && req.method === "POST"){
		res.writeHead(200, {'Content-Type': 'text/plain'});
		res.write("Add a course to our resources.");
		res.end();
	}
	// updateCourse + PUT
	else if(req.url === "/updateCourse" && req.method === "PUT"){
		res.writeHead(200, {'Content-Type': 'text/plain'});
		res.write("Update a course in our resources.");
		res.end();
	}
	// archiveCourses + DELETE
	else if(req.url === "/archiveCourse" && req.method === "DELETE"){
		res.writeHead(200, {'Content-Type': 'text/plain'});
		res.write("Archive a course in our resources.");
		res.end();
	} else {
		res.writeHead(404, {'Content-Type': 'text/plain'});
		res.write("ERROR: Double-check response or code base");
		res.end();
	}


}).listen(port);

console.log(`Server now active at port ${port}.`);